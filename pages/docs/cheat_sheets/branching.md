# Branching 
### Klon project til egen pc
    git clone (repository)
Sørg for at ssh ind på gitlab
### Opret branch
    git branch (navn på branch du vil oprette)

### Check hvilken branch du er i
    git branch
Viser med grøn tekst, hvilken branch du befinder dig i

### Skift branch
    git checkout (navn på branch)

### Afslut
    git add 
    git commit -m"(note på hvad du har lavet)"
    git push --set-upstream origin (navn på branch)

### Merging

***For at merge, log ind på gitlab og lav et "merge request". Efter det kan du selv merge din branch sammen med main, eller få en anden maintainer til det***

### Pull fra main branch
    git pull origin main
